upgrade: clean build install

clean:
	rm dist/*

build:
	python -m build

upload:
	python3 -m twine upload dist/*

install:
	pip install ./dist/*.tar.gz	
