#!/usr/bin/env python
from setuptools import setup

setup(install_requires=['qrcode', 'cli_ui', 'darkdetect'])
